CREATE TABLE USER_FILE(
    IDFile Bigint NOT NULL AUTO_INCREMENT,
    filename VARCHAR(255) NOT NULL,
    content Text NOT NULL,
    isNew boolean default true,
    nameSchool VARCHAR(255) DEFAULT "",
    idsession VARCHAR(255) DEFAULT "",
    PRIMARY KEY (IDFile)
);

CREATE TABLE USER_DATA(
    IDUser Bigint NOT NULL AUTO_INCREMENT,
    code VARCHAR(120) NOT NULL,
    username VARCHAR(120) NOT NULL,
    pass VARCHAR(120) NOT NULL,
    idsession VARCHAR(10) NOT NULL,
    idcloud VARCHAR(120) NOT NULL,
    isUpdated boolean default false,
    CONSTRAINT uCode UNIQUE (username,pass,idsession,idcloud),
    PRIMARY KEY (IDUser)
);
