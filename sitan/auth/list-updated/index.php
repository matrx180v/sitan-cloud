<?php
include __DIR__.'/../../../dao/pool.php';

if($_SERVER['REQUEST_METHOD'] != 'POST' && $_SERVER['REQUEST_METHOD'] != 'GET' ){
    print(json_encode(array(
        'error' => 'Bad operation'
    )));
    return ;
}

$idsession = isset($_GET["idsession"])?$_GET["idsession"]:$_POST["idsession"];
$idcloud = isset($_GET["idcloud"])?$_GET["idcloud"]:$_POST["idcloud"];

$pool = new Pool();
$result = $pool->listAuthDataUpdated($idsession, $idcloud);
$pool->setStatusAuthDataUpdated($idsession, $idcloud);

print(json_encode($result));