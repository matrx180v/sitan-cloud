<?php
include __DIR__.'/../../dao/pool.php';

if($_SERVER['REQUEST_METHOD'] != 'POST' && $_SERVER['REQUEST_METHOD'] != 'GET' ){
    print(json_encode(array(
        'error' => 'Bad operation'
    )));
    return ;
}

$username = isset($_GET["u"])?$_GET["u"]:$_POST["u"];
$password = isset($_GET["p"])?$_GET["p"]:$_POST["p"];
$idsession = isset($_GET["i"])?$_GET["i"]:$_POST["i"];
$idcloud = isset($_GET["_i"])?$_GET["_i"]:$_POST["_i"];

$pool = new Pool();
$result = $pool->getUserData($username, $password, $idsession, $idcloud);

if($result!=null){
    print("ok");
}else{
    print("failed");
}