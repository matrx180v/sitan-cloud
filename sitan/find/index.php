<?php
include __DIR__.'/../../dao/pool.php';

if($_SERVER['REQUEST_METHOD'] != 'POST' && $_SERVER['REQUEST_METHOD'] != 'GET' ){
    print(json_encode(array(
        'error' => 'Bad operation'
    )));
    return ;
}

$filename = isset($_GET["name"])?$_GET["name"]:$_POST["name"];
$fetchStrong = isset($_GET["fetchStrong"])?$_GET["fetchStrong"]:$_POST["fetchStrong"];

if(!isset($filename)){
    print('failed');
    return;
}

$pool = new Pool();
$result = $pool->listFileByName($filename, $fetchStrong);

print(json_encode($result));