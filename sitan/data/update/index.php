<?php
include __DIR__.'/../../../dao/pool.php';

if($_SERVER['REQUEST_METHOD'] != 'POST' && $_SERVER['REQUEST_METHOD'] != 'GET' ){
    print(json_encode(array(
        'error' => 'Bad operation'
    )));
    return ;
}

$oldFilename = isset($_GET["oldFilename"])?$_GET["oldFilename"]:$_POST["oldFilename"];
$newFilename = isset($_GET["newFilename"])?$_GET["newFilename"]:$_POST["newFilename"];

$pool = new Pool();
$result = $pool->updateFileByName($oldFilename, $newFilename);

print("ok");