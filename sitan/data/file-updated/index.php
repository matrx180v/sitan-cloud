<?php
include __DIR__.'/../../../dao/pool.php';
include_once __DIR__.'/../../../env/Env.php';

if($_SERVER['REQUEST_METHOD'] != 'POST' && $_SERVER['REQUEST_METHOD'] != 'GET' ){
    print(json_encode(array(
        'error' => 'Bad operation'
    )));
    return ;
}

$prefix = Env::getValueParameter("prefix");
$status = Env::getValueParameter("status");
$action = Env::getValueParameter("action");

$pool = new Pool();

if($action=='set-status'){
    print($pool->updateStatusFileName($filename, 'true', $status));
}else if($action=='list'){
    $datas = $pool->listNewFileByName($prefix,'true');
    $rows = [];
    for($i=0; $i<count($datas); $i++){
        $data = $datas[$i];
        $filename = $data['filename'];
        $file = $pool->getFileByName($filename);
        if($file!=null){
            array_push($rows, $file->toJson());
        }
    }
    print(json_encode($rows));
}
