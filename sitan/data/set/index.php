<?php
include __DIR__.'/../../../dao/pool.php';
include_once __DIR__.'/../../../env/Env.php';

if($_SERVER['REQUEST_METHOD'] != 'POST' && $_SERVER['REQUEST_METHOD'] != 'GET' ){
    print(json_encode(array(
        'error' => 'Bad operation'
    )));
    return ;
}

$filename = Env::getValueParameter("filename");
$content = Env::getValueParameter("content");
$isFromServer = Env::getValueParameter("server");

$pool = new Pool();
$file = $pool->setFile($filename, $content);

if($file!=null){
    if(!isset($isFromServer)){
        print($pool->updateStatusFile($file->id, true));
    }else{
        print("ok");
    }    
}else{
    print("failed");
}