<?php
include __DIR__.'/../../../dao/pool.php';

if($_SERVER['REQUEST_METHOD'] != 'POST' && $_SERVER['REQUEST_METHOD'] != 'GET' ){
    print(json_encode(array(
        'error' => 'Bad operation'
    )));
    return ;
}

$oldpart = isset($_GET["oldpart"])?$_GET["oldpart"]:$_POST["oldpart"];
$newpart = isset($_GET["newpart"])?$_GET["newpart"]:$_POST["newpart"];
$olduser = isset($_GET["olduser"])?$_GET["olduser"]:$_POST["olduser"];
$newuser = isset($_GET["newuser"])?$_GET["newuser"]:$_POST["newuser"];
$oldpass = isset($_GET["oldpass"])?$_GET["oldpass"]:$_POST["oldpass"];
$newpass = isset($_GET["newpass"])?$_GET["newpass"]:$_POST["newpass"];
$code = isset($_GET["code"])?$_GET["code"]:$_POST["code"];
$idsession_cloud = isset($_GET["idsession_cloud"])?$_GET["idsession_cloud"]:$_POST["idsession_cloud"];
$idsession = isset($_GET["idsession"])?$_GET["idsession"]:$_POST["idsession"];
$isFromServer = isset($_GET["server"])?$_GET["server"]:$_POST["server"];

if(!isset($oldpart)||!isset($newpart)||!isset($olduser)||!isset($newuser)
||!isset($oldpass)||!isset($newpass)||!isset($idsession)||!isset($idsession_cloud)){
    print("error");
    return;
}

$pool = new Pool();

$result = $pool->updateDataAuthUser($code, $oldpart, $newpart,$olduser,$newuser,$oldpass,$newpass,$idsession,$idsession_cloud,isset($isFromServer));

if(!$result){
    print("failed");
}else{
    print("ok");
}