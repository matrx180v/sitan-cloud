<?php
include __DIR__.'/../../../dao/pool.php';
include_once __DIR__.'/../../../env/Env.php';

if($_SERVER['REQUEST_METHOD'] != 'POST' && $_SERVER['REQUEST_METHOD'] != 'GET' ){
    print(json_encode(array(
        'error' => 'Bad operation'
    )));
    return ;
}

$filename = Env::getValueParameter("filename");
$isFromServer = Env::getValueParameter("server");

$pool = new Pool();
$file = isset($isFromServer)?$pool->getNewFileByName($filename):$pool->getFileByName($filename);
if($file!=null){
    if(isset($isFromServer)){
        if($pool->updateStatusFile($file->id, false)=='ok'){
            print($file->content);
        }else{
            print("failed");
        }
    }else{
        print($file->content);
    }     
}else{
    print("failed");
}
