<?php
include __DIR__.'/../../../dao/pool.php';
include_once __DIR__.'/../../../env/Env.php';

if($_SERVER['REQUEST_METHOD'] != 'POST' && $_SERVER['REQUEST_METHOD'] != 'GET' ){
    print(json_encode(array(
        'error' => 'Bad operation'
    )));
    return ;
}

$filename = Env::getValueParameter("filename");

$path = __DIR__.'/../../../'.$filename;
if(file_exists($path)){
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.basename($path).'"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($path));
    readfile($path);
    die();
}else{
    //throw new Exception('Fichier de la version introuvable!!!');
    header("HTTP/1.0 404 Not Found");
} 
