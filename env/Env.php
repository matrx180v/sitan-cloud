<?php
class ENV{
    public static $isLocalhost = true;

    public static $urlLocalHost = "http://localhost:8081/depot";
    public static $urlInternetHost = "https://sitanmirror.000webhostapp.com";

    public static $contextLocalHost = "/depot";
    public static $contextInternetHost = "";

    public static $hostLocalDB = "localhost";
    public static $portLocalDB = "3306";
    public static $nameLocalDB = "mirror";
    public static $userLocalDB = "root";
    public static $passLocalDB = "azerty";
    
    public static $hostInternetDB = "localhost";
    public static $portInternetDB = "3306";
    public static $nameInternetDB = "id19371345_db_mirror";
    public static $userInternetDB = "id19371345_user_mirror";
    public static $passInternetDB = "Azerty@1234567890";

    public static function getLocalConnection(){
        $conn = mysqli_connect(ENV::$hostLocalDB, ENV::$userLocalDB, ENV::$passLocalDB, ENV::$nameLocalDB);
        return $conn;
    }

    public static function getInternetConnection(){
        $conn  =  new mysqli(ENV::$hostInternetDB, ENV::$userInternetDB, ENV::$passInternetDB, ENV::$nameInternetDB, ENV::$portInternetDB);
        return $conn;
    }

    public static function getConnection(){
        if(ENV::$isLocalhost){
            return ENV::getLocalConnection();
        }else{
            return ENV::getInternetConnection();
        }
    }

    public static function getURLHost(){
        if(ENV::$isLocalhost){
            return ENV::$urlLocalHost;
        }else{
            return ENV::$urlInternetHost;
        }
    }

    public static function getcontextHost(){
        if(ENV::$isLocalhost){
            return ENV::$contextLocalHost;
        }else{
            return ENV::$contextInternetHost;
        }
    }

    public static function endsWith( $haystack, $needle ) {
        $length = strlen( $needle );
        if( !$length ) {
            return true;
        }
        return substr( $haystack, -$length ) === $needle;
    }

    public static function encodeSHA256($src){
        $code = base64_encode(hash('sha256', $src, true));
        if(ENV::endsWith($code, "=")){
            return $code;
        }
        return $code."=";
    }

    public static function getValueParameter($name){
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == 'POST'){
            if(isset($_POST[$name])){
                return $_POST[$name];
            }
        } elseif ($method == 'GET'){
            if(isset($_GET[$name])){
                return $_GET[$name];
            }
        }
        $json = file_get_contents('php://input');
        $data = json_decode($json);
        if($data[$name]!=null){
            return $data[$name];
        }
        return null;
    }

}