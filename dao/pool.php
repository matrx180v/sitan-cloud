<?php

require_once 'cache.data.php';
include __DIR__.'/entities/File.php';
include __DIR__.'/entities/User.php';
include_once __DIR__.'/../env/Env.php';

class Pool{

    public static function getDataInCacheMemory($index){ 
        $cacheMemory = new Cache();
        $result = $cacheMemory->retrieve($index);
        if($result==null){
            return null;
        }
        return $result;
    } 

    public static function setDataInCacheMemory($index, $data){
        $cacheMemory = new Cache();
        $cacheMemory->store($index, $data);
    }

    public static function resetDataInCacheMemory(){
        $cacheMemory = new Cache();
        $cacheMemory->eraseAll();
    }

    function resetCacheData(){
        Pool::resetDataInCacheMemory();
        return array(
            "response" => "ok"
        );
    }

    function getContentCacheData(){
        $cacheMemory = new Cache();
        return array(
            "response" => $cacheMemory->retrieveAll()
        );
    }

    function startTransaction($conn){
        $status = $conn->query("BEGIN;");
        return $status;
    }

    function endTransaction($conn){
        $status = $conn->query("COMMIT;");
        return $status;
    }

    function cancelTransaction($conn){
        $status = $conn->query("ROLLBACK;");
        return $status;
    }

    function setIdCloud($oldIdcloud, $newIdcloud){
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return null;
        }
    }

    function updateFileByName($oldFilename, $newFilename){
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return null;
        }
        $file = new File();
        $query = $file->updateFilename($oldFilename,$newFilename);
        $status = $conn->query($query);
        if(!$status) {
            return null;
        }
        Pool::resetDataInCacheMemory();
        return "ok";
    }

    function updateStatusFile($id, $status){
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return null;
        }
        $file = new File();
        $query = $file->updateStatusFile($id, $status);
        $status = $conn->query($query);
        if(!$status) {
            return null;
        }
        Pool::resetDataInCacheMemory();
        return "ok";
    }

    function updateStatusFileName($filename, $typeFetch, $status){
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return null;
        }
        $file = new File();
        $query = $file->updateStatusFileName($filename, $typeFetch, $status);
        $status = $conn->query($query);
        if(!$status) {
            return null;
        }
        Pool::resetDataInCacheMemory();
        return "ok";
    }

    
    
    function listFileByName($filename, $typeFetch){
        $result = Pool::getDataInCacheMemory('listFileByName-'.$filename.'-'.$typeFetch);
        if($result!=null){
            return $result;
        }
        $rows = array();
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return $rows;
        }
        $file = new File();
        $query = $file->listFileByName($filename, $typeFetch=="true");
        try{
            $sth = mysqli_query($conn, $query);
            if (mysqli_num_rows($sth) > 0) {
                while($row = mysqli_fetch_assoc($sth)) {
                    array_push($rows, array(
                        'id' => $row['IDFile'],
                        'filename' => $row['filename']
                    ));
                }
            }
        } catch (Exception $e) {}
        Pool::setDataInCacheMemory('listFileByName-'.$filename.'-'.$typeFetch, $rows);
        return $rows;
    }

    function listNewFileByName($filename, $typeFetch){
        $result = Pool::getDataInCacheMemory('listNewFileByName-'.$filename.'-'.$typeFetch);
        if($result!=null){
            return $result;
        }
        $rows = array();
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return $rows;
        }
        $file = new File();
        $query = $file->listNewFileByName($filename, $typeFetch=="true");
        try{
            $sth = mysqli_query($conn, $query);
            if (mysqli_num_rows($sth) > 0) {
                while($row = mysqli_fetch_assoc($sth)) {
                    array_push($rows, array(
                        'id' => $row['IDFile'],
                        'filename' => $row['filename']
                    ));
                }
            }
        } catch (Exception $e) {}
        Pool::setDataInCacheMemory('listNewFileByName-'.$filename.'-'.$typeFetch, $rows);
        return $rows;
    }
    
    function getFileByName($filename){
        $result = Pool::getDataInCacheMemory('getFileByName-'.$filename);
        if($result!=null){
            $file = new File();
            return $file->setFromJson($result);
        }
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return null;
        }
        $file = new File();
        $query = $file->getFileByName($filename);
        $sth = mysqli_query($conn, $query);
        while($row = mysqli_fetch_assoc($sth)) {
            $file = $file->setFromResultSet($row);
            Pool::setDataInCacheMemory('getFileByName-'.$filename, $file->toJson());
            return $file;
        }
        
        return null;
    }

    function getNewFileByName($filename){
        $result = Pool::getDataInCacheMemory('getNewFileByName-'.$filename);
        if($result!=null){
            $file = new File();
            return $file->setFromJson($result);
        }
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return null;
        }
        $file = new File();
        $query = $file->getNewFileByName($filename);
        $sth = mysqli_query($conn, $query);
        while($row = mysqli_fetch_assoc($sth)) {
            $file = $file->setFromResultSet($row);
            Pool::setDataInCacheMemory('getNewFileByName-'.$filename, $file->toJson());
            return $file;
        }
        
        return null;
    }

    function setFile($filename, $content){
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return null;
        }
        $file = $this->getFileByName($filename);
        $query = null;
        if($file==null){
            $file = new File();
            $query = $file->addFile($filename, $content);
        }else{
            $query = $file->updateFile($file->id, $filename, $content);
        }
        $status = $conn->query($query);
        if(!$status) {
            echo "FindFileByName:".$status;
            return null;
        }
        
        $file = $this->getFileByName($filename);
        Pool::resetDataInCacheMemory();
        return $file;        
    }

    function renameFile($oldpart, $newpart){
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return null;
        }
        $file = new File();
        $query = $file->renameFile($oldpart, $newpart);
        $status = $conn->query($query);
        if(!$status) {
            return null;
        }
        Pool::resetDataInCacheMemory();
        return "ok";        
    }

    function getUserData($username, $pass, $idsession, $idcloud){
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return null;
        }

        $user = new User();
        $query = $user->getUserData($username, $pass, $idsession, $idcloud);

        $status = $conn->query($query);
        if(!$status) {
            return null;
        }
        Pool::resetDataInCacheMemory();
        return "ok";
    }

    /*function updateUser($newuser, $newpass, $olduser, $oldpass, $idsession, $idcloud, $isFromServer){
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return null;
        }
        $user = new User();
        $query = "";//$user->updateAuthData($newuser, $newpass, $olduser, $oldpass, $idsession, $idcloud, $isFromServer);
        $status = $conn->query($query);
        if(!$status) {
            return null;
        }
        Pool::resetDataInCacheMemory();
        return "ok";        
    }*/

    
    function updateDataAuthUser($code, $oldpart, $newpart,$olduser,$newuser,$oldpass,$newpass,$idsession,$idsession_cloud,$isFromServer){

        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return false;
        }

        $this->startTransaction($conn);

        $file = new File();
        $query = $file->renameFile($oldpart, $newpart);
        $status = $conn->query($query);
        if(!$status) {
            $this->cancelTransaction($conn);
            return false;
        }

        $user = new User();
        $query = $user->updateAuthData($code, $newuser, $newpass, $olduser, $oldpass, $idsession, $idsession_cloud, $isFromServer);
        $status = $conn->query($query);
        if(!$status) {
            $this->cancelTransaction($conn);
            return false;
        }

        $this->endTransaction($conn);
        Pool::resetDataInCacheMemory();
        return true;
    }

    function setStatusAuthDataUpdated($idsession, $idcloud){
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return false;
        }
        $user = new User();
        $query = $user->setStatusAuthDataUpdated($idsession, $idcloud);
        $status = $conn->query($query);
        if(!$status) {
            return false;
        }
        Pool::resetDataInCacheMemory();
        return true;
    }
    

    function listAuthDataUpdated($idsession, $idcloud){
        $result = Pool::getDataInCacheMemory('listAuthDataUpdated-'.$idsession.'-'.$idcloud);
        if($result!=null){
            return $result;
        }
        $rows = array();
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return $rows;
        }
        $user = new User();
        $query = $user->listAuthDataUpdated($idsession, $idcloud);
        try{
            $sth = mysqli_query($conn, $query);
            if (mysqli_num_rows($sth) > 0) {
                while($row = mysqli_fetch_assoc($sth)) {
                    array_push($rows, $row);
                }
            }
        } catch (Exception $e) {}
        Pool::setDataInCacheMemory('listAuthDataUpdated-'.$idsession.'-'.$idcloud, $rows);
        return $rows;
    }
}