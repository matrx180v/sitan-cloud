<?php

class User{
    public $id;
    public $code;
    public $username;
    public $pass;
    public $idsession;
    public $idcloud;


    function toJson(){
        $env = new ENV();
        return array(
            'id' => $this->id,
            'code' => $this->code,
            'username' => $this->username,
            'pass' => $this->pass,
            'idsession' => $this->idsession,
            'idcloud' => $this->idcloud
        );
    }

    function setFromJson($json){
        $this->id = $json['id'];
        $this->code = $json['code'];
        $this->username = $json['username'];
        $this->pass = $json['pass'];
        $this->idsession = $json['idsession'];
        $this->idcloud = $json['idcloud'];
        return $this;
    }

    function setFromResultSet($row){
        $this->id = $row['IDUser'];
        $this->code = $row['code'];
        $this->username = $row['username'];
        $this->pass = $row['pass'];
        $this->idsession = $row['idsession'];
        $this->idcloud = $row['idcloud'];
        return $this;
    }

    function addUser($username, $pass, $idsession, $idcloud){
        return "INSERT INTO USER_DATA(username,pass,idsession,idcloud) VALUES ('".preg_replace("/'/", "''", $username)."','".preg_replace("/'/", "''", $pass)."','".preg_replace("/'/", "''", $idsession)."','".preg_replace("/'/", "''", $idcloud)."')";
    }

    function setStatusAuthDataUpdated($idsession, $idcloud){
        return "UPDATE USER_DATA SET isUpdated=false WHERE isUpdated=true AND idsession='".preg_replace("/'/", "''", $idsession)."' AND idcloud='".preg_replace("/'/", "''", $idcloud)."'";
    }

    function setIdCloud($oldIdcloud, $newIdcloud){
        return "UPDATE USER_DATA SET idcloud='".preg_replace("/'/", "''", $newIdcloud)."' WHERE idcloud='".preg_replace("/'/", "''", $oldIdcloud)."'";
    }

    function updateAuthData($code, $username, $pass, $oldusername, $oldpass, $idsession, $idcloud, $isFromServer){
        return "UPDATE USER_DATA SET code='".preg_replace("/'/", "''", $code)."', username='".preg_replace("/'/", "''", $username)."',pass='".preg_replace("/'/", "''", $pass)."', isUpdated=".$isFromServer." WHERE idsession='".preg_replace("/'/", "''", $idsession)."' AND idcloud='".preg_replace("/'/", "''", $idcloud)."' AND username='".preg_replace("/'/", "''", $oldusername)."' AND pass='".preg_replace("/'/", "''", $oldpass)."'";
    }

    function listAuthDataUpdated($idsession, $idcloud){
        return "SELECT * FROM USER_DATA WHERE isUpdated=true AND idsession='".preg_replace("/'/", "''", $idsession)."' AND idcloud='".preg_replace("/'/", "''", $idcloud)."' ";
    }
    
    function getUserData($username, $pass, $idsession, $idcloud){
        return "SELECT * FROM USER_DATA WHERE username='".preg_replace("/'/", "''", $username)."' AND pass='".preg_replace("/'/", "''", $pass)."' AND idsession='".preg_replace("/'/", "''", $idsession)."' AND idcloud='".preg_replace("/'/", "''", $idcloud)."' ";
    }
}