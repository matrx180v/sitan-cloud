<?php

class File{
    public $id;
    public $filename;
    public $content;
    public $isNew;


    function toJson(){
        $env = new ENV();
        return array(
            'id' => $this->id,
            'filename' => $this->filename,
            'content' => $this->content,
            'isNew' => $this->isNew
        );
    }

    function setFromJson($json){
        $this->id = $json['id'];
        $this->filename = $json['filename'];
        $this->content = $json['content'];
        $this->isNew = $json['isNew'];
        return $this;
    }

    function setFromResultSet($row){
        $this->id = $row['IDFile'];
        $this->filename = $row['filename'];
        $this->content = $row['content'];
        $this->isNew = $row['isNew'];
        return $this;
    }

    function addFile($filename, $content){
        return "INSERT INTO USER_FILE(filename,content) VALUES ('".preg_replace("/'/", "''", $filename)."','".preg_replace("/'/", "''", $content)."')";
    }

    function updateFile($id, $filename, $content){
        return "UPDATE USER_FILE SET filename='".preg_replace("/'/", "''", $filename)."',content='".preg_replace("/'/", "''", $content)."' WHERE IDFile=".$id;
    }

    function renameFile($fromString, $newString){
        return "UPDATE USER_FILE SET filename=replace(filename, '".preg_replace("/'/", "''", $fromString)."', '".preg_replace("/'/", "''", $newString)."') WHERE filename like '%".preg_replace("/'/", "''", $fromString)."%'";
    }

    function updateFilename($oldfilename, $newfilename){
        return $this->renameFile($oldfilename, $newfilename);
    }

    function deleteFile($id){
        return "DELETE FROM USER_FILE WHERE IDFile=".$id;
    }

    function getFile($id){
        return "SELECT * FROM USER_FILE WHERE IDFile=".$id;
    }

    function getFileByName($filename){
        return "SELECT * FROM USER_FILE WHERE filename='".preg_replace("/'/", "''", $filename)."'";
    }

    function getNewFileByName($filename){
        return "SELECT * FROM USER_FILE WHERE isNew=true AND filename='".preg_replace("/'/", "''", $filename)."'";
    }

    function listFileByName($filename, $typeFetch){
        $motif = $filename;
        $motif = preg_replace("/\s+/", " ", $motif);
        $parts = explode(" ", $motif);
        $motif = "";
        for($i=0; $i<count($parts);$i++){
            $part = $parts[$i];
            $part = preg_replace("/'/", "''", $part);
            $part = strtolower($part);
            $motif = $motif." ".($typeFetch?"AND":"OR")." lower(filename) like '%".$part."%'";
        }
        return "SELECT * FROM USER_FILE WHERE 1=1 ".$motif." ORDER BY filename  ";
    }

    function listNewFileByName($filename, $typeFetch){
        $motif = $filename;
        $motif = preg_replace("/\s+/", " ", $motif);
        $parts = explode(" ", $motif);
        $motif = "";
        for($i=0; $i<count($parts);$i++){
            $part = $parts[$i];
            $part = preg_replace("/'/", "''", $part);
            $part = strtolower($part);
            $motif = $motif." ".($typeFetch?"AND":"OR")." lower(filename) like '%".$part."%'";
        }
        return "SELECT * FROM USER_FILE WHERE isNew=true ".$motif." ORDER BY filename  ";
    }

    function updateStatusFile($id, $status){
        return "UPDATE USER_FILE SET isNew=".$status." WHERE IDFile=".$id;
    }

    function updateStatusFileName($filename, $typeFetch, $status){
        $motif = $filename;
        $motif = preg_replace("/\s+/", " ", $motif);
        $parts = explode(" ", $motif);
        $motif = "";
        for($i=0; $i<count($parts);$i++){
            $part = $parts[$i];
            $part = preg_replace("/'/", "''", $part);
            $part = strtolower($part);
            $motif = $motif." ".($typeFetch?"AND":"OR")." lower(filename) like '%".$part."%'";
        }
        return "UPDATE USER_FILE SET isNew=".$status." WHERE 1=1 ".$motif;
    }
}